
// requiring the express, mongoose and route
const express = require('express');
const app = express();
const cors       = require('cors');
const bodyParser = require('body-parser');
const route = require('./router/routing');

//for static files
app.use(express.static(__dirname));

//used as bodyParser
app.use(express.json());

//using the route 
app.use(route);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

//declaring dynamic port or assign it 3000
const port = process.env.PORT || 3000;

//listening to the port
app.listen(port, ()=>{
    console.log("listening... " +port);
});

