angular.module('app', []).controller('myController', function ($scope, $http) {
    

    $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
        $('#repository').DataTable().destroy();
        $('#repository').DataTable({
            "paging":true
        });
    })

    $scope.getname = function() {
    $scope.repository_list = "";
    var request = {
        method: 'POST',
        url: '/api',
        data: {
                'githubname': $scope.githubname

            },
        headers: {
            'Content-Type': "application/json"
        }
    };
     $http(request)
        .then(function (res) {
            if(res.data.code==200)
            $scope.repository_list = res.data.data
            else if(res.data.code==404){
                toastr.error('Data Not Available')
            $scope.repository_list = []; 
            }                                   
        })
    }


    $scope.get_graph = function (name) {
        var request = {
        method: 'POST',
        url: '/api/repository',
        data: {
                'githubname': $scope.githubname,
                'reponame': name

            },
        headers: {
            'Content-Type': "application/json"
        }
    };
        $http(request)
            .then(function (res) {
                if(res.data.code==200){
                    $scope.graph_data = res.data.data;  
                    load_chart(res.data.data) 
                }else if(res.data.code==404){
                    toastr.error('Data Not Available')
                    $scope.graph_data = []; 
                }
               
				
            })
    };
})
.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});