function load_chart(data) {
  var container = $('#pagination-ex');
  var options = {
    dataSource: data.all,
    callback: function (response, pagination) {
      console.log(pagination);
      plot_graph(response, data, pagination)
    }
  };

  container.pagination(options);

}

function plot_graph(check, data, pagination) {
  var date_array = [];
  for (var i = 1; i <= pagination.totalNumber; i++) {
    date_array.push(i)
  }


  var from = (pagination.pageNumber * parseInt(pagination.pageSize)) - parseInt(pagination.pageSize)
  var to = pagination.pageNumber * parseInt(pagination.pageSize)
  var all = data.all.slice(from, to);
  var owner = data.owner.slice(from, to);
  var x_axis = date_array.slice(from, to);
  var chartOptions = {
    responsive: true,
    legend: {
      position: "top"
    },
    title: {
      display: true,
      text: "Weekly Commits Owner vs All"
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }



  var barChartData = {
    labels: x_axis,
    datasets: [

      {
        label: "owner",
        backgroundColor: "lightblue",
        borderColor: "blue",
        borderWidth: 1,
        data: owner
      },
      {
        label: "all",
        backgroundColor: "lightgreen",
        borderColor: "green",
        borderWidth: 1,
        data: all
      }

    ]
  };
  var ctx = document.getElementById("canvas").getContext("2d");
  window.myBar = new Chart(ctx, {
    type: "bar",
    data: barChartData,
    options: chartOptions
  });

}