//importing express
const express = require('express');

//importing router module
const router = express.Router();
//importing node-rest-client npm
var axios = require('axios')
//getting all the data router
router.post('/api', async (req, res) => {
	var githubname = req.body.githubname;
	var apiUrl = "https://api.github.com/users/" + githubname + "/repos"
	await axios.get(apiUrl)
		.then(function (response) {
			res.send({ "code": 200, "data": response.data })
		}).catch(function (error) {
			res.send({ "code": 404, "data": "" })
		});



});


router.post('/api/repository', async (req, res) => {
	var githubname = req.body.githubname;
	var reponame = req.body.reponame;
	var apiUrl = "https://api.github.com/repos/" + githubname + "/" + reponame + "/stats/participation"
	await axios.get(apiUrl)
		.then(function (response) {	
			res.send({ "code": 200, "data": response.data })
		}).catch(function (error) {
			res.send({ "code": 404, "data": "" })
		});

});

//exporting the router
module.exports = router;